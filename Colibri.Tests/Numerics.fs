module Colibri.Tests.Numerics

open System
open Xunit
open Colibri.Numerics

module Rational =

    [<Fact>]
    let Zero () =
        Assert.Equal(Rational.Zero, Rational.FromInteger 0)

    [<Fact>]
    let One () =
        Assert.Equal(Rational.One, Rational.FromInteger 1)

    [<Fact>]
    let Simplify () =
        Assert.Equal({ num = 0; den = 1 }, { num = 0; den = 2 }.Simplify)
        Assert.Equal({ num = 3; den = 2 }, { num = 6; den = 4 }.Simplify)

    [<Fact>]
    let ``Simplify negative denominator`` () =
        Assert.Equal({ num = -1; den = 4 }, { num = 1; den = -4 }.Simplify)

    [<Fact>]
    let ``Conversion to double`` () =
        Assert.Equal(0.25, { num = 1; den = 4 }.ToDouble())

    [<Fact>]
    let ``Conversion to string`` () =
        Assert.Equal("-1/4", { num = -1; den = 4 }.ToString())

    [<Fact>]
    let ``FSharp Structural format`` () =
        let my_rational = { num = -1; den = 4 }
        Assert.Equal("-1/4", $"%A{my_rational}")

    [<Fact>]
    let Addition () =
        Assert.Equal({ num = 7; den = 12 }, { num = 1; den = 4 } + { num = 1; den = 3 })

    [<Fact>]
    let Negation () =
        Assert.Equal({ num = -1; den = 4 }, -{ num = 1; den = 4 })

    [<Fact>]
    let Subtraction () =
        Assert.Equal({ num = -1; den = 12 }, { num = 1; den = 4 } - { num = 1; den = 3 })

    [<Fact>]
    let Reciprocal () =
        Assert.Equal({ num = 4; den = 1 }, Rational.Reciprocal { num = 1; den = 4 })
        Assert.Equal({ num = -4; den = 1 }, Rational.Reciprocal { num = -1; den = 4 })

    [<Fact>]
    let Multiplication () =
        Assert.Equal({ num = 2; den = 3 }, { num = 2; den = 1 } * { num = 1; den = 3 })
        Assert.Equal({ num = 2; den = 3 }, { num = 10; den = 1 } * { num = 1; den = 15 })
        Assert.Equal({ num = 2; den = 3 }, { num = 10; den = 7 } * { num = 7; den = 15 })

    [<Fact>]
    let Division () =
        Assert.Equal({ num = 2; den = 3 }, { num = 2; den = 1 } / { num = 3; den = 1 })
        Assert.Equal({ num = 4; den = 3 }, { num = 1; den = 1 } / { num = 3; den = 4 })
        Assert.Equal({ num = 3; den = 2 }, { num = 7; den = 10 } / { num = 7; den = 15 })

    [<Fact>]
    let Powers () =
        Assert.Equal({ num = 4; den = 9 }, Rational.Pow({ num = 2; den = 3 }, 2))
        Assert.Equal({ num = 27; den = 8 }, Rational.Pow({ num = 2; den = 3 }, -3))


    [<Fact>]
    let Logarithms () =
        Assert.Equal(Double.Log(2.0 / 3.0), Rational.Log { num = 2; den = 3 }, 14)
        Assert.Equal(3.0, Rational.Log({ num = 8; den = 1 }, 2), 14)
        Assert.Equal(-3.0, Rational.Log({ num = 1; den = 8 }, 2), 14)
        Assert.Equal(2.0, Rational.Log10({ num = 100; den = 1 }), 14)
        Assert.Equal(-2.0, Rational.Log10({ num = 1; den = 100 }), 14)

    [<Fact>]
    let Comparison () =
        Assert.True({ num = 1; den = 3 } > { num = 1; den = 4 })

        Assert.Equal<List<Rational>>(
            [ { num = 1; den = 4 }
              { num = 1; den = 3 } ],
            (List.sort [ { num = 1; den = 3 }
                         { num = 1; den = 4 } ])
        )
