module Colibri.Tests.Objects

open Xunit
open Colibri.Objects
open Colibri.Objects.Eval

module Eval =
    [<Fact>]
    let rec Environments () =
        let env_a = Environment.Null

        let env_b =
            env_a.Extend(
                NameSource.LocalScopeFromMap(
                    Map [| ("a", Datum.Symbol "1" |> ref)
                           ("b", Datum.Symbol "2" |> ref) |]
                )
            )

        let env_c =
            env_b.Extend(NameSource.LocalScopeFromMap(Map [| ("a", Datum.Symbol "3" |> ref) |]))


        let test_lookup (env: Environment) name =
            (env.Lookup name)
            |> Option.map (fun x -> x.Value)

        match test_lookup env_b "a" with
        | Some(Datum.Symbol "1") -> ()
        | _ -> Assert.Fail "unexpected lookup value"
        
        match test_lookup env_b "b" with
        | Some(Datum.Symbol "2") -> ()
        | _ -> Assert.Fail "unexpected lookup value"
        
        match test_lookup env_c "a" with
        | Some(Datum.Symbol "3") -> ()
        | _ -> Assert.Fail "unexpected lookup value"
        
        match test_lookup env_c "b" with
        | Some(Datum.Symbol "2") -> ()
        | _ -> Assert.Fail "unexpected lookup value"
        
        match test_lookup env_c "c" with
        | None -> ()
        | _ -> Assert.Fail "unexpected lookup value"
        
        //
        // test_lookup with env_b "b" (Some(Datum.Symbol "2"))
        // test_lookup env_c "a" (Some(Datum.Symbol "3"))
        // test_lookup env_c "b" (Some(Datum.Symbol "2"))
