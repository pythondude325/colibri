module Colibri.Lexer

open System
open System.Collections.Generic
open System.Globalization
open System.Text

open Colibri.Reporting
open Colibri.Span
open Colibri.Numerics


let identifier_subsequent c =
    match c with
    | '\u200c'
    | '\u200d' -> true
    | _ ->
        let char_category =
            Char.GetUnicodeCategory c

        match char_category with
        | UnicodeCategory.UppercaseLetter // Lu
        | UnicodeCategory.LowercaseLetter // Ll
        | UnicodeCategory.TitlecaseLetter // Lt
        | UnicodeCategory.ModifierLetter // Lm
        | UnicodeCategory.OtherLetter // Lo
        | UnicodeCategory.NonSpacingMark // Mn
        | UnicodeCategory.SpacingCombiningMark // Mc
        | UnicodeCategory.EnclosingMark // Me
        | UnicodeCategory.DecimalDigitNumber // Nd
        | UnicodeCategory.LetterNumber // Nl
        | UnicodeCategory.OtherNumber // No
        | UnicodeCategory.DashPunctuation // Pd
        | UnicodeCategory.ConnectorPunctuation // Pc
        | UnicodeCategory.OtherPunctuation // Po
        | UnicodeCategory.CurrencySymbol // Sc
        | UnicodeCategory.MathSymbol // Sm
        | UnicodeCategory.ModifierSymbol // Sk
        | UnicodeCategory.OtherSymbol // So
        | UnicodeCategory.PrivateUse -> true
        | _ -> false

let rec identifier_initial c =
    match c with
    | '-'
    | '+'
    | '.' -> false
    | _ ->
        let char_category =
            Char.GetUnicodeCategory c

        match char_category with
        | UnicodeCategory.DecimalDigitNumber
        | UnicodeCategory.EnclosingMark
        | UnicodeCategory.SpacingCombiningMark -> false
        | _ -> identifier_subsequent c

let identifier_sign_subsequent c =
    match c with
    | '-'
    | '+'
    | '@'
    | _ -> identifier_initial c

let identifier_dot_subsequent c =
    c = '.' || (identifier_sign_subsequent c)

let isDelimiter c =
    match c with
    | '|'
    | '('
    | ')'
    | '"'
    | ';' -> true
    | c when Char.IsWhiteSpace(c) -> true
    | _ -> false

let (|StartsWith|_|) (prefix: ReadOnlySpan<char>) (str: ReadOnlySpan<char>) =
    if str.StartsWith(prefix) then
        Some()
    else
        None

[<Struct>]
type LexerErrorKind =
    | InvalidToken
    | UnexpectedEOI
    | InvalidStringEscape
    | InvalidCharacter of name: string
    | InvalidSymbolEscape

[<Struct>]
type LexerError =
    { kind: LexerErrorKind
      loc: FileSpan }

    static member FromSpanned(spanned: Spanned<LexerErrorKind>) =
        { kind = spanned.value
          loc = spanned.span }

    member this.SomeError () : LexerResult<'T> option  = this |> Error |> Some

    interface IReportable with
        member this.GenerateReport =
            match this.kind with
            | InvalidToken ->
                Report.Error
                    {| Text = "invalid token"
                       Location = this.loc |}
            | UnexpectedEOI ->
                Report.Error
                    {| Text = "unexpected end of input"
                       Location = this.loc |}
            | InvalidStringEscape ->
                Report.Error
                    {| Text = "invalid string escape"
                       Location = this.loc |}
            | InvalidCharacter name ->
                Report.Error
                    {| Text = $"Invalid character name `{name}`"
                       Location = this.loc |}
            | InvalidSymbolEscape ->
                Report.Error
                    {| Text = "invalid symbol escape"
                       Location = this.loc |}

and LexerResult<'T> = Result<'T, LexerError>

[<RequireQualifiedAccess>]
type TokenKind =
    | Identifier of name: string
    | BooleanTrue
    | BooleanFalse
    | Char of value: char
    | String of value: string
    | BeginList
    | EndList
    | BeginVector
    | BeginByteVector
    | Quote
    | QuasiQuote
    | Unquote
    | SpliceUnquote
    | Dot
    | LineComment
    | ExpressionComment
    | NestedComment
    | Number of value: Number

type Token = Spanned<TokenKind>

type NumExactness =
    | Exact
    | Inexact

    static member FromPrefix(c: char) : option<NumExactness> =
        match c with
        | 'e' -> Some Exact
        | 'i' -> Some Inexact
        | _ -> None

type NumRadix =
    | Binary
    | Octal
    | Decimal
    | Hexadecimal

    static member FromPrefix(c: char) : option<NumRadix> =
        match c with
        | 'b' -> Some Binary
        | 'o' -> Some Octal
        | 'd' -> Some Decimal
        | 'x' -> Some Hexadecimal
        | _ -> None

    member this.ReadDigit(c: char) : option<Integer> =
        match this with
        | Binary ->
            match c with
            | '0' -> Some 0
            | '1' -> Some 1
            | _ -> None
        | Octal ->
            match c with
            | '0' -> Some 0
            | '1' -> Some 1
            | '2' -> Some 2
            | '3' -> Some 3
            | '4' -> Some 4
            | '5' -> Some 5
            | '6' -> Some 6
            | '7' -> Some 7
            | _ -> None
        | Decimal ->
            match c with
            | '0' -> Some 0
            | '1' -> Some 1
            | '2' -> Some 2
            | '3' -> Some 3
            | '4' -> Some 4
            | '5' -> Some 5
            | '6' -> Some 6
            | '7' -> Some 7
            | '8' -> Some 8
            | '9' -> Some 9
            | _ -> None
        | Hexadecimal ->
            match c with
            | '0' -> Some 0
            | '1' -> Some 1
            | '2' -> Some 2
            | '3' -> Some 3
            | '4' -> Some 4
            | '5' -> Some 5
            | '6' -> Some 6
            | '7' -> Some 7
            | '8' -> Some 8
            | '9' -> Some 9
            | 'a'
            | 'A' -> Some 10
            | 'b'
            | 'B' -> Some 11
            | 'c'
            | 'C' -> Some 12
            | 'd'
            | 'D' -> Some 13
            | 'e'
            | 'E' -> Some 14
            | 'f'
            | 'F' -> Some 15
            | _ -> None

    member this.Base: Integer =
        match this with
        | Binary -> 2
        | Octal -> 8
        | Decimal -> 10
        | Hexadecimal -> 16

type NumPrefix =
    | Exactness of NumExactness
    | Radix of NumRadix

    static member FromPrefix(c: char) : LexerResult<NumPrefix> =
        match NumExactness.FromPrefix c with
        | Some exactness -> Ok <| Exactness exactness
        | None ->
            match NumRadix.FromPrefix c with
            | Some radix -> Ok <| Radix radix
            | None -> failwith "todo error"

type NumSign =
    | Plus
    | Minus

type NumParsingState =
    { exactness: option<NumExactness>
      radix: option<NumRadix>
      sign: option<NumSign>
      value: option<Integer> }

    static member empty =
        { exactness = None
          radix = None
          sign = None
          value = None }

    member this.UpdateExactness(value: NumExactness) : LexerResult<NumParsingState> =
        match this.exactness with
        | Some _ -> failwith "todo error"
        | None -> Ok { this with exactness = Some value }

    member this.UpdateRadix(value: NumRadix) : LexerResult<NumParsingState> =
        match this.radix with
        | Some _ -> failwith "todo error"
        | None -> Ok { this with radix = Some value }

    member this.UpdatePrefix(value: NumPrefix) : LexerResult<NumParsingState> =
        match value with
        | Exactness exactness -> this.UpdateExactness exactness
        | Radix radix -> this.UpdateRadix radix

[<RequireQualifiedAccess>]
type private LexerEnumeratorState =
    | Begin
    | Token of Token
    | Error of LexerErrorKind
    | End

type Lexer =
    { source: FileSource
      str: string
      offset: int }

    static member FromFileSource(source: FileSource) : Lexer =
        { source = source
          str = source.contents
          offset = 0 }


    member this.StrSpan: ReadOnlySpan<char> =
        this.str.AsSpan().Slice(start = this.offset)

    member this.MakeSpan(length: int) : FileSpan =
        { source = this.source
          start = this.offset
          length = length }

    member this.MakeSpan(start: int, length: int) : FileSpan =
        { source = this.source
          start = this.offset + start
          length = length }

    member this.Advance(length: int) : Lexer =
        { this with offset = this.offset + length }

    member this.GiveToken (kind: TokenKind) (length: int) : Option<LexerResult<Lexer * Token>> =
        (this.Advance length,
         { span = this.MakeSpan length
           value = kind })
        |> LexerResult.Ok
        |> Some

    member this.TakeToken() : Option<LexerResult<Lexer * Token>> =
        if this.StrSpan.Length = 0 then
            None
        else
            match this.StrSpan[0] with
            | ' '
            | '\n'
            | '\t' -> this.Advance(1).TakeToken()
            | '(' -> this.GiveToken TokenKind.BeginList 1
            | ')' -> this.GiveToken TokenKind.EndList 1
            | '\'' -> this.GiveToken TokenKind.Quote 1
            | '`' -> this.GiveToken TokenKind.QuasiQuote 1
            | ',' ->
                match this.StrSpan with
                | StartsWith ",@" -> this.GiveToken TokenKind.SpliceUnquote 2
                | _ -> this.GiveToken TokenKind.Unquote 1
            | '"' -> // String
                let string_escapes =
                    Map [ ('a', '\a')
                          ('b', '\b')
                          ('t', '\t')
                          ('n', '\n')
                          ('r', '\r')
                          ('"', '"')
                          ('\\', '\\') ]

                let rec loop (p: int) (b: StringBuilder) : LexerResult<Lexer * Token> option =
                    if p < this.StrSpan.Length then
                        match this.StrSpan[p] with
                        | '"' -> this.GiveToken (TokenKind.String(b.ToString())) (p + 1)
                        | '\\' ->
                            if this.StrSpan.Length >= (p + 1) then
                                match Map.tryFind (this.StrSpan[p + 1]) string_escapes with
                                | Some c -> loop (p + 2) (b.Append c)
                                | None ->
                                    { loc = this.MakeSpan(p, 2)
                                      kind = InvalidStringEscape }
                                        .SomeError()
                            else
                                { loc = this.MakeSpan(p, 0)
                                  kind = UnexpectedEOI }
                                    .SomeError()
                        | c -> loop (p + 1) (b.Append c)
                    else
                        { loc = this.MakeSpan(p, 0)
                          kind = UnexpectedEOI }
                            .SomeError()

                loop 1 (StringBuilder())
            | ';' -> // Line comment
                let line_end = this.StrSpan.IndexOf('\n')
                this.GiveToken TokenKind.LineComment line_end
            | '#' ->
                match this.StrSpan with
                | StartsWith "#(" -> this.GiveToken TokenKind.BeginVector 2
                | StartsWith "#u8(" -> this.GiveToken TokenKind.BeginByteVector 4
                | StartsWith "#false" -> this.GiveToken TokenKind.BooleanFalse 6
                | StartsWith "#f" -> this.GiveToken TokenKind.BooleanFalse 2
                | StartsWith "#true" -> this.GiveToken TokenKind.BooleanTrue 5
                | StartsWith "#t" -> this.GiveToken TokenKind.BooleanTrue 2
                | StartsWith "#;" -> this.GiveToken TokenKind.ExpressionComment 2
                | StartsWith "#\\" ->
                    let delimiter_pos =
                        let rec loop i =
                            if i >= this.StrSpan.Length then
                                i
                            else if isDelimiter this.StrSpan[i] then
                                i
                            else
                                loop (i + 1)

                        loop 2

                    let character_name =
                        String(this.StrSpan.Slice(start = 2, length = delimiter_pos - 2))

                    if character_name.Length = 1 then
                        this.GiveToken (TokenKind.Char character_name[0]) delimiter_pos
                    else
                        let character_names =
                            Map [ ("alarm", '\a')
                                  ("backspace", '\b')
                                  ("delete", '\x7f')
                                  ("escape", '\x1b')
                                  ("newline", '\n')
                                  ("null", '\x00')
                                  ("return", '\r')
                                  ("space", ' ')
                                  ("tab", '\t') ]

                        match Map.tryFind character_name character_names with
                        | Some c -> this.GiveToken (TokenKind.Char c) delimiter_pos
                        | None ->
                            { loc = this.MakeSpan(delimiter_pos)
                              kind = InvalidCharacter character_name }
                                .SomeError()
                | StartsWith "#|" ->
                    let rec loop level pos =
                        if level = 0 then
                            this.GiveToken TokenKind.NestedComment pos
                        else if pos < this.StrSpan.Length then
                            match this.StrSpan.Slice(pos) with
                            | StartsWith "#|" -> loop (level + 1) (pos + 2)
                            | StartsWith "|#" -> loop (level - 1) (pos + 2)
                            | _ -> loop level (pos + 1)
                        else
                            { loc = this.MakeSpan(pos, 0)
                              kind = UnexpectedEOI }.SomeError()

                    loop 1 2
                | StartsWith "#i"
                | StartsWith "#e"
                | StartsWith "#b"
                | StartsWith "#o"
                | StartsWith "#d"
                | StartsWith "#x" -> this.ParseNumber()
                | _ ->
                    { loc = this.MakeSpan(2)
                      kind = InvalidToken }
                        .SomeError()
            | '.' -> // TODO: need to add the number stuff in here
                match this.StrSpan[1] with
                | c when identifier_dot_subsequent c -> this.ParseIdentifier()
                | _ -> this.GiveToken TokenKind.Dot 1
            | '-'
            | '+'
            | '0'
            | '1'
            | '2'
            | '3'
            | '4'
            | '5'
            | '6'
            | '7'
            | '8'
            | '9' -> this.ParseNumber()
            | '|' -> // Delimited identifier
                let rec loop i elements =
                    if i >= this.StrSpan.Length then
                        { loc = this.MakeSpan(i, 0)
                          kind = UnexpectedEOI }
                            .SomeError()
                    else
                        match this.StrSpan[i] with
                        | '|' ->
                            this.GiveToken
                                (List.rev elements
                                 |> Array.ofList
                                 |> String
                                 |> TokenKind.Identifier)
                                (i + 1)
                        | '\\' ->
                            if i + 1 >= this.StrSpan.Length then
                                { loc = this.MakeSpan(i + 1, 0)
                                  kind = UnexpectedEOI }
                                    .SomeError()
                            else
                                match this.StrSpan[i + 1] with
                                | 'x' -> failwith "todo: parse hex escape"
                                | '|' -> loop (i + 2) ('|' :: elements)
                                | 'a' -> loop (i + 2) ('\a' :: elements)
                                | 'b' -> loop (i + 2) ('\b' :: elements)
                                | 't' -> loop (i + 2) ('\t' :: elements)
                                | 'n' -> loop (i + 2) ('\n' :: elements)
                                | 'r' -> loop (i + 2) ('\r' :: elements)
                                | _ ->
                                    { loc = this.MakeSpan(i, 2)
                                      kind = InvalidSymbolEscape }
                                        .SomeError()
                        | c -> loop (i + 1) (c :: elements)

                loop 1 []
            | c when identifier_initial c -> // Identifier
                this.ParseIdentifier()
            | _ ->
                { loc = this.MakeSpan(0, 1)
                  kind = InvalidToken }.SomeError()

    member this.GetSeq() =
        Seq.unfold
            (Option.bind (fun (lexer: Lexer) ->
                match lexer.TakeToken() with
                | Some (Ok (lexer, token)) -> Some(Ok token, Some lexer)
                | Some (Error e) -> Some(Error e, None)
                | None -> None))
            (Some this)

    member this.ParseIdentifier() =
        let delimiter_pos =
            let rec loop i =
                if i >= this.StrSpan.Length then
                    i
                else if isDelimiter this.StrSpan[i] then
                    i
                else
                    loop (i + 1)

            loop 0

        let identifier_str =
            String(this.StrSpan.Slice(0, delimiter_pos))

        let all_valid_chars =
            identifier_str
            |> Seq.skip 1
            |> Seq.map identifier_subsequent
            |> Seq.fold (&&) true

        if all_valid_chars then
            this.GiveToken (TokenKind.Identifier identifier_str) delimiter_pos
        else
            { loc = this.MakeSpan(delimiter_pos)
              kind = InvalidToken }.SomeError()

    member this.ParseNumber() =
        let start = this.offset

        this.parse_num ()
        |> Result.map (fun (lexer, state) ->
            let value = Option.get state.value

            let value =
                match state.sign with
                | Some Plus -> value
                | Some Minus -> -value
                | None -> failwith "unreachable"

            let span =
                this.MakeSpan(lexer.offset - start)

            let token =
                { span = span
                  value = TokenKind.Number(Number.Int value) }

            (lexer, token))
        |> Some

    member this.parse_num() : LexerResult<Lexer * NumParsingState> =
        Ok(this, NumParsingState.empty)
        |> Result.bind Lexer.parse_num_prefix
        |> Result.bind Lexer.parse_num_prefix
        |> Result.map Lexer.parse_num_prefix_defaults
        |> Result.bind Lexer.parse_num_sign
        |> Result.bind Lexer.parse_digits

    static member parse_num_prefix((lexer, state): Lexer * NumParsingState) : LexerResult<Lexer * NumParsingState> =
        if lexer.StrSpan[0] = '#' then
            NumPrefix.FromPrefix lexer.StrSpan[1]
            |> Result.bind state.UpdatePrefix
            |> Result.map (fun state -> (lexer.Advance 2, state))
        else
            Ok(lexer, state)

    static member parse_num_prefix_defaults((lexer, state): Lexer * NumParsingState) : Lexer * NumParsingState =
        match state.radix with
        | None -> (lexer, { state with radix = Some Decimal })
        | _ -> (lexer, state)

    static member parse_num_sign((lexer, state): Lexer * NumParsingState) : LexerResult<Lexer * NumParsingState> =
        match lexer.StrSpan[0] with
        | '+' -> Ok(lexer.Advance 1, { state with sign = Some Plus })
        | '-' -> Ok(lexer.Advance 1, { state with sign = Some Minus })
        | _ -> Ok(lexer, { state with sign = Some Plus })

    static member parse_digits((lexer, state): Lexer * NumParsingState) : LexerResult<Lexer * NumParsingState> =
        match state.radix with
        | Some radix -> Ok radix
        | None -> failwith "todo error"
        |> Result.bind (fun radix ->
            let rec loop (lexer: Lexer) value =
                match radix.ReadDigit lexer.StrSpan[0] with
                | Some digit -> loop (lexer.Advance 1) (value * radix.Base + digit)
                | None -> (lexer, value)

            match radix.ReadDigit lexer.StrSpan[0] with
            | Some digit ->
                let (lexer, value) =
                    loop (lexer.Advance 1) digit

                Ok(lexer, { state with value = Some value })
            | None -> failwith "todo error")

    interface IEnumerable<LexerResult<Token>> with
        member this.GetEnumerator() : IEnumerator<LexerResult<Token>> = this.GetSeq().GetEnumerator()
        member this.GetEnumerator() : Collections.IEnumerator = this.GetSeq().GetEnumerator()
