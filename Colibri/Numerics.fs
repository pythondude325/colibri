module Colibri.Numerics

open System
open System.Numerics

// We will just use BigInteger as our integer numeric type because BigInteger does not make a heap allocation as long as
// the value is within the range int.MinValue < x <= int.MaxValue.
// https://source.dot.net/#System.Runtime.Numerics/System/Numerics/BigInteger.cs,42
type Integer = BigInteger

[<Struct>]
[<StructuredFormatDisplay("{DisplayText}")>]
[<StructuralEquality; CustomComparison>]
type Rational =
    { num: Integer
      den: Integer }

    static member Zero = { num = 0; den = 1 }
    static member One = { num = 1; den = 1 }

    static member FromInteger(i: Integer) = { num = i; den = 1 }

    static member op_Implicit = Rational.FromInteger

    member this.DisplayText = this.ToString()

    override this.ToString() = sprintf $"%A{this.num}/%A{this.den}"

    member this.ToDouble() : double =
        // TODO: this will behave incorrectly if the numerator or denominator are out of the double range, but the final
        // value wouldn't
        (double this.num) / (double this.den)

    member this.Simplify =
        let d =
            Integer.GreatestCommonDivisor(this.num, this.den)

        if this.den.Sign = -1 then
            { num = -this.num / d
              den = -this.den / d }
        else
            { num = this.num / d
              den = this.den / d }

    static member (+)(a: Rational, b: Rational) =
        let d =
            Integer.GreatestCommonDivisor(a.den, b.den)

        { num = a.num * b.den / d + b.num * a.den / d
          den = a.den * b.den / d }

    static member (~-)(a: Rational) = { num = -a.num; den = a.den }

    static member (-)(a: Rational, b: Rational) = a + -b

    static member Reciprocal(a: Rational) = { num = a.den; den = a.num }.Simplify

    static member (*)(a: Rational, b: Rational) =
        { num = a.num * b.num
          den = a.den * b.den }
            .Simplify

    static member (/)(a: Rational, b: Rational) = a * Rational.Reciprocal b

    static member Pow(a: Rational, b: int) =
        if b < 0 then
            Rational.Pow(Rational.Reciprocal a, -b)
        else
            { num = Integer.Pow(a.num, b)
              den = Integer.Pow(a.den, b) }

    static member Log(a: Rational) : double =
        (Integer.Log a.num) - (Integer.Log a.den)

    static member Log(a: Rational, b: double) : double =
        (Integer.Log(a.num, b)) - (Integer.Log(a.den, b))

    static member Log10(a: Rational) : double = Rational.Log(a, 10.0)

    member this.CompareTo other =
        let d =
            Integer.GreatestCommonDivisor(this.den, other.den)

        let av = this.num * other.den * d
        let bv = other.num * this.den * d
        av.CompareTo bv
        
    interface IComparable with
        member this.CompareTo(obj) =
            match obj with
            | :? Rational as other ->
                this.CompareTo other
            | _ ->
                invalidArg (nameof obj) "Object is not a rational"


    interface IComparable<Rational> with
        member this.CompareTo(other: Rational) = this.CompareTo other

[<Struct>]
type Complex<'T> = { real: 'T; imag: 'T }

[<RequireQualifiedAccess>]
type Number =
    | Int of Integer
    | Rational of Rational
    | Real of double
    | ComplexInt of Complex<Integer>
    | ComplexRational of Complex<Rational>
    | ComplexFloat of Complex<double>
