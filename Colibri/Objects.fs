module rec Colibri.Objects

open Colibri.Numerics
open Colibri.Span

[<RequireQualifiedAccess>]
type Datum =
    | Null
    | Pair of head: Datum * tail: Datum
    | Vector of items: Datum array
    | ByteVector of items: byte array
    | Symbol of name: string
    | Boolean of value: bool
    | Char of value: char
    | String of value: string
    | Number of value: Number
    | Procedure of proc: Procedure
    | Syntax of syn: Syntax

    static member List: Datum seq -> Datum =
        Seq.rev
        >> Seq.fold (flip (curry Datum.Pair)) Datum.Null

    member this.AsSyntax(location: FileSpan) : Datum =
        Datum.Syntax {datum = this; location = location; macro = false}
        
    member this.GetLocation : FileSpan option =
        match this with
        | Datum.Syntax s -> Some s.location
        | _ -> None

    member this.Len : int =
        match this with
        | Datum.Null -> 0
        | Datum.Pair(_, tail) -> 1 + tail.Len
        | _ -> 1

    static member ToList(datum: Datum) : Datum list =
        match datum with
        | Datum.Null -> []
        | Datum.Pair (head, tail) -> head :: (Datum.ToList tail)
        | _ -> [ datum ]



let curry f a b = f (a, b)

// F# requires very specific annotations otherwise it only allows one monomorphization?
let flip<'a, 'b, 'c> (f: 'b -> 'a -> 'c) (a: 'a) (b: 'b) : 'c = f b a

let rec datum_to_list (datum: Datum) : Datum list =
    match datum with
    | Datum.Pair (head, tail) -> head :: datum_to_list tail
    | Datum.Null -> []
    | _ -> failwith "bad list"



let (|DatumList|_|) (datum: Datum) =
    match datum with
    | Datum.Pair (head, tail) ->
        match tail with
        | DatumList rest -> Some(head :: rest)
        | _ -> None
    | Datum.Null -> Some []
    | _ -> None

[<RequireQualifiedAccess>]
type Procedure =
    | Builtin of func: (Datum list -> Eval.EvalResult<Datum array>)
    | Lambda of env: Eval.Environment * formals: Datum * body: Datum list

    member this.Invoke(args: Datum list) : Eval.EvalResult<Datum array> =
        match this with
        | Procedure.Builtin f -> f args
        | Procedure.Lambda (env, formals, body) ->
            let rec bind_args map formals (args: Datum list) =
                match formals with
                | Datum.Symbol name -> Ok(Map.add name (ref (Datum.List args)) map)
                | Datum.Pair (Datum.Symbol name, rest) ->
                    match args with
                    | args_head :: args_tail -> bind_args (Map.add name (ref args_head) map) rest args_tail
                    | [] -> Error Eval.EvalError.ExpectedArgument
                | Datum.Null ->
                    match args with
                    | [] -> Ok map
                    | _ -> Error Eval.EvalError.TooManyArguments
                | _ -> Error Eval.EvalError.BadLambdaFormal


            bind_args Map.empty formals args
            |> Result.bind (fun arg_map ->
                let lambda_env = env.Extend (Eval.NameSource.LocalScopeFromMap arg_map)

                let rec eval_loop body =
                    match body with
                    | [ body_end ] -> Eval.eval body_end lambda_env
                    | body_head :: body_tail ->
                        Eval.eval body_head lambda_env
                        |> Result.bind (fun _ -> eval_loop body_tail)
                    | [] ->
                        // this should probably give a proper error anyways, but theoretically this should be
                        // unreachable because we are never supposed to have an empty list for the lambda body
                        failwith "unreachable"

                eval_loop body)


type Syntax =
    { datum: Datum
      location: FileSpan
      macro: bool }

    member this.Span = this.location

    member this.ToSpanned =
        this.location.With this.datum


let rec syntax_to_datum (datum: Datum) =
    match datum with
    | Datum.Syntax { datum = inner } -> syntax_to_datum inner
    | Datum.Pair (a, b) -> Datum.Pair(syntax_to_datum a, syntax_to_datum b)
    | Datum.Vector arr -> Datum.Vector(Array.map syntax_to_datum arr)
    | d -> d

module Eval =
    open Macros

    type EvalError =
        | UnknownOperator of name: string
        | InvalidArgument of Datum
        | InvalidArguments of Datum array
        | UnboundVariable of name: string
        | ExpectedArgument
        | TooManyArguments
        | BadLambdaFormal
        | FailedToMatchMacro of Datum
        | Todo
        
    type EvalResult<'T> = Result<'T, EvalError>
            
    [<Struct>]
    [<RequireQualifiedAccess>]
    type NameSource =
        | LocalScope of vars: Map<string, Datum ref> * macros: Map<string, SyntaxRules>
        | Library
        
        static member LocalScopeFromMap (map: Map<string, Datum ref>) : NameSource =
            LocalScope (map, Map.empty)
            
        static member LocalScopeFromMap (map: Map<string, SyntaxRules>) : NameSource =
            LocalScope (Map.empty, map)
        
        member this.Lookup (name: string) : Datum ref option =
            match this with
            | LocalScope (vars, _) -> Map.tryFind name vars
            | Library -> None
            
        member this.MacroLookup (name: string) : SyntaxRules option =
            match this with
            | LocalScope (_, macros) -> Map.tryFind name macros
            | Library -> None
            
    [<RequireQualifiedAccess>]
    type Environment =
        | Null
        | Chain of source: NameSource * parent: Environment
        
        member this.Extend (source: NameSource) : Environment =
            Environment.Chain (source, this)
        
        member this.Lookup (name: string) : Datum ref option =
            match this with
            | Null -> None
            | Chain (source, parent) ->
                source.Lookup name
                |> Option.orElseWith (fun () -> parent.Lookup name)
                
        member this.MacroLookup (name: string) : SyntaxRules option =
            match this with
            | Null -> None
            | Chain (source, parent) ->
                source.MacroLookup name
                |> Option.orElseWith (fun () -> parent.MacroLookup name)

    // We should only need to implement these with the following primitive expressions:
    // - [x] 4.1.1 Variable references
    // - [x] 4.1.2 Literal Expressions (including `quote`)
    // - [x] 4.1.3 Procedure calls
    // - [x] 4.1.4 `lambda`
    // - [x] 4.1.5 `if`
    // - [x] 4.1.6 `set!`
    // - [ ] 4.1.7 Inclusion
    let rec eval (datum: Datum) (env: Environment) : EvalResult<Datum array> =
        match datum with
        | Datum.Pair (Datum.Symbol "if", operands) ->
            match operands with
            | DatumList [ test; consequent ] ->
                match eval test env with
                | Ok [| Datum.Boolean false |] -> Ok Array.empty
                | Ok [| _ |] -> eval consequent env
                | Ok a -> Error(InvalidArguments a)
                | Error e -> Error e
            | DatumList [ test; consequent; alternate ] ->
                match eval test env with
                | Ok [| Datum.Boolean false |] -> eval alternate env
                | Ok [| _ |] -> eval consequent env
                | Ok a -> Error(InvalidArguments a)
                | Error e -> Error e
            | _ -> Error(InvalidArgument operands)
        | Datum.Pair (Datum.Symbol "quote", operands) ->
            match operands with
            | DatumList [ datum ] -> Ok [| datum |]
            | _ -> InvalidArgument operands |> Error
        | Datum.Pair (Datum.Symbol "lambda", operands) ->
            match operands with
            | DatumList (formals :: body) ->
                Procedure.Lambda(env, formals, body)
                |> Datum.Procedure
                |> Array.singleton
                |> Ok
            | _ -> InvalidArgument operands |> Error
        | Datum.Pair (Datum.Symbol "set!", operands) ->
            match operands with
            | DatumList [ Datum.Symbol name; value ] ->
                match env.Lookup name with
                | Some var ->
                    match eval value env with
                    | Ok [| datum |] -> Ok datum
                    | Ok a -> InvalidArguments a |> Error
                    | Error e -> Error e
                    |> Result.map (fun v -> var.Value <- v)
                    |> Result.map (fun () -> Array.empty)
                | None -> UnboundVariable name |> Error
            | _ -> InvalidArgument operands |> Error
        | Datum.Pair (Datum.Symbol "let", Datum.Pair (bindings, body)) ->
            let collect_bindings acc binding =
                match binding with
                | DatumList [ Datum.Symbol name; expr ] -> Result.map (fun l -> (Datum.Symbol name, expr) :: l) acc
                | _ -> Error Todo

            Datum.ToList bindings
            |> List.fold collect_bindings (Ok [])
            |> Result.map List.unzip
            |> Result.map (fun (identifiers, exprs) ->
                let lambda = Datum.Pair (Datum.Symbol "lambda", Datum.Pair (Datum.List identifiers, body))
                Datum.Pair (lambda, Datum.List exprs)
            )
            |> Result.bind ((flip eval) env)
        | Datum.Pair (Datum.Symbol "begin", Datum.Pair (head, tail)) ->
            match tail with
            | Datum.Null -> eval head env
            | _ ->
                let begin_ = Datum.Pair (Datum.Symbol "begin", tail)
                let lambda = Datum.List [Datum.Symbol "lambda"; Datum.Null; begin_]
                Datum.List [lambda; head] |> (flip eval) env
        | Datum.Pair (Datum.Symbol "let-syntax", operands) ->
            match operands with
            | DatumList [ bindings; body ] -> Ok {| bindings = bindings; body = body |}
            | _ -> failwith "todo error"
            |> Result.bind (fun context ->
                let rec loop (bindings: Datum) : EvalResult<Map<string, SyntaxRules>> =
                    match bindings with
                    | Datum.Null -> Ok Map.empty
                    | Datum.Pair (DatumList [ Datum.Symbol keyword; rules ], tail) ->
                        loop tail
                        |> Result.bind (fun tail ->
                            eval_syntax_rules rules
                            |> Result.map (fun rules -> map_merge tail <| Map [ (keyword, rules) ]))
                    | _ -> failwith "todo error"

                loop context.bindings |> Result.map (fun bindings -> {| context with bindings = bindings |}))
            |> Result.bind begin fun context ->
                printfn $"(debug) %A{context.bindings}"
                eval context.body (env.Extend (NameSource.LocalScopeFromMap context.bindings))
            end
        | Datum.Pair(proc, operands) ->
            match proc with
            | Datum.Symbol name ->
                env.MacroLookup name
                |> Option.map (Macros.apply operands >> Result.bind (fun expansion_result -> eval expansion_result env))
            | _ -> None
            |> Option.defaultWith begin fun () ->
                match eval proc env with
                | Ok [| Datum.Procedure proc |] ->
                    let rec fold_results s =
                        match Seq.tryHead s with
                        | Some(Ok e) ->
                            match fold_results (Seq.tail s) with
                            | Ok t -> Ok(e :: t)
                            | Error e -> Error e
                        | Some(Error e) -> Error e
                        | None -> Ok []

                    (datum_to_list operands)
                    |> Seq.map (fun a -> eval a env)
                    |> fold_results
                    |> Result.map (Array.concat >> List.ofArray)
                    |> Result.bind proc.Invoke

                | Ok e -> InvalidArguments e |> Error // Should be procedure call
                | Error e -> Error e
            end
        | Datum.Symbol var ->
            match env.Lookup var with
            | Some value -> Ok [| value.Value |]
            | None -> UnboundVariable var |> Error
        | _ -> [| datum |] |> Ok

    module Macros =

        let map_merge<'a, 'b when 'a: comparison> (map1: Map<'a, 'b>) (map2: Map<'a, 'b>) : Map<'a, 'b> =
            Map.fold (fun acc key value -> Map.add key value acc) map1 map2

        type SyntaxRule = Pattern * Template

        type SyntaxRules = SyntaxRule array

        let eval_syntax_rules (datum: Datum) : EvalResult<SyntaxRules> =
            match datum with
            | Datum.Pair (Datum.Symbol "syntax-rules", body) -> Ok body
            | _ -> failwith "todo error"
            |> Result.bind (function
                | Datum.Pair (Datum.Symbol ellipsis, Datum.Pair (literals, rules)) -> Ok(ellipsis, literals, rules)
                | Datum.Pair (literals, rules) -> Ok("...", literals, rules)
                | _ -> failwith "todo error")
            |> Result.bind (fun (ellipsis, literals, rules) ->
                let rec list_literals datum =
                    match datum with
                    | Datum.Pair (head, tail) ->
                        match head with
                        | Datum.Symbol name -> list_literals tail |> Result.map (Set.add name)
                        | _ -> failwith "todo error"
                    | Datum.Null -> Ok Set.empty
                    | _ -> failwith "todo error" in

                list_literals literals
                |> Result.map (fun literals -> (ellipsis, literals, rules)))
            |> Result.bind (fun (ellipsis, literals, rules) ->
                let rec loop rules =
                    match rules with
                    | Datum.Pair (DatumList [ pattern; template ], tail) ->
                        Pattern.parse ellipsis literals pattern
                        |> Result.bind (fun pattern ->
                            Template.fromDatum ellipsis (Pattern.Variables pattern) template
                            |> Result.map (fun template -> (pattern, template)))
                        |> Result.bind (fun rule ->
                            loop tail
                            |> Result.map (fun rules -> rule :: rules))
                    | Datum.Null -> Ok List.empty
                    | _ -> failwith "todo error" in loop rules |> Result.map Array.ofList)

        // makes it easy to debug print things by composing this function with other stuff
        // e.g. Option.map (dbg >> template.instantiate)
        let dbg (value: 'a) : 'a =
            printfn $"(debug) %A{value}"
            value

        let apply (datum: Datum) (rules: SyntaxRules) : EvalResult<Datum> =
            let picker ((pattern, template) : SyntaxRule) =
                pattern.matches datum |> Option.map (template.instantiate >> dbg)

            match Array.tryPick picker rules with
            | Some datum -> Ok datum
            | None -> Error <| FailedToMatchMacro datum

        type Pattern =
            | Underscore
            | Constant of value: Datum
            | LiteralIdent of name: string
            | Variable of name: string
            | List of value: PatternList

            static member Variables (pattern: Pattern) : Set<string> =
                match pattern with
                | Underscore | Constant _ | LiteralIdent _ -> Set.empty
                | Variable name -> Set.singleton name
                | List value -> PatternList.Variables value

            // Returns the mapping of pattern variables to datums
            member this.matches(datum: Datum) : Map<string, Match> option =
                match this with
                | Underscore -> Some Map.empty
                | Constant value ->
                    match (value, datum) with
                    | (Datum.Null, Datum.Null) -> Some Map.empty
                    | (Datum.Boolean a, Datum.Boolean b) when a = b -> Some Map.empty
                    | (Datum.Char a, Datum.Char b) when a = b -> Some Map.empty
                    | (Datum.String a, Datum.String b) when a = b -> Some Map.empty
                    | (Datum.Number a, Datum.Number b) when a = b -> Some Map.empty
                    | _ -> None
                | LiteralIdent ident ->
                    match datum with
                    | Datum.Symbol name when name = ident -> Some Map.empty
                    | _ -> None
                | Variable name -> Some <| Map [ (name, Single datum) ]
                | List l -> l.matches datum

            static member parse (ellipsis: string) (identifiers: Set<string>) (datum: Datum) : EvalResult<Pattern> =
                (* From r7rs:
                    - The <pattern> in a <syntax rule> is a list <pattern> whose first element is an identifier.
                    - The keyword at the beginning of the pattern in a <syntax rule> is not involved in the matching
                      and is considered neither a pattern variable nor a literal identifier.
                *)
                match datum with
                | Datum.Pair (Datum.Symbol _, tail) -> Pattern.fromDatum ellipsis identifiers tail
                | _ -> failwith "todo error"

            static member fromDatum (ellipsis: string) (identifiers: Set<string>) (datum: Datum) : EvalResult<Pattern> =
                match datum with
                | Datum.Symbol name ->
                    match name with
                    | "_" -> Ok Underscore
                    | _ ->
                        if Set.contains name identifiers then
                            LiteralIdent name
                        else
                            Variable name
                        |> Ok
                | Datum.Pair _ ->
                    PatternList.fromDatum ellipsis identifiers datum
                    |> Result.map List
                | _ -> Ok <| Constant datum

        and PatternList =
            { patterns: Pattern list
              ellipsis: (Pattern * Pattern list) option }

            static member Variables (pattern_list: PatternList) : Set<string> =
                seq {
                    yield! Seq.ofList pattern_list.patterns
                    match pattern_list.ellipsis with
                    | Some (head, tail) ->
                        yield head
                        yield! Seq.ofList tail
                    | None -> ()
                }
                |> Seq.fold (fun s -> Pattern.Variables >> Set.union s) Set.empty

            member this.matches(datum: Datum) : Map<string, Match> option =
                let expression = Datum.ToList datum

                let rec match_list (patterns: Pattern list) (datums: Datum list) (k: Map<string, Match> -> Datum list -> 'a option) : 'a option =
                    let rec loop (patterns: Pattern list) datums k acc =
                        match patterns, datums with
                        | [], _ -> k acc datums
                        | _, [] -> None
                        | pattern_head :: pattern_tail, datum_head :: datum_tail ->
                            pattern_head.matches datum_head |> Option.bind (map_merge acc >> loop pattern_tail datum_tail k)
                    in
                    loop patterns datums k Map.empty

                let match_ellipsis (ellipsis_pattern, rest) (datums : Datum list) : Map<string, Match> option =
                    let variables = Pattern.Variables ellipsis_pattern |> Set.toArray
                    let ellipsis_map_merge (maps : Map<string, Match> list) : Map<string, Match> =
                        Array.map (fun variable ->
                            let values = List.map (Map.find variable) maps |> Match.Ellipsis in
                            (variable, values)
                        ) variables
                        |> Map.ofArray

                    let rec loop (datums: Datum list) n acc k =
                        match n with
                        | 0 -> k (ellipsis_map_merge acc) datums
                        | _ ->
                            match datums with
                            | datum_head :: datum_tail ->
                                ellipsis_pattern.matches datum_head |> Option.bind (fun head -> loop datum_tail (n-1) (head :: acc) k)
                            | [] -> failwith "unreachable"

                    let ellipsis_length = (List.length datums) - (List.length rest)
                    if ellipsis_length < 0 then
                        failwith "todo error" // not enough elements to match on
                    else
                        loop datums ellipsis_length [] (fun matches datums ->
                            match_list rest datums (fun m _ -> Some m)
                            |> Option.map (map_merge matches)
                        )

                match_list this.patterns (Datum.ToList datum) (fun matches datums -> 
                    match this.ellipsis with
                    | Some ellipsis -> match_ellipsis ellipsis datums |> Option.map (map_merge matches)
                    | None -> Some matches
                )

            static member fromDatum
                (ellipsis: string)
                (identifiers: Set<string>)
                (datum: Datum)
                : EvalResult<PatternList> =
                let rec loop datum =
                    match datum with
                    | Datum.Pair (head, Datum.Pair (Datum.Symbol name, tail)) when name = ellipsis ->
                        loop tail
                        |> Result.bind (fun tail ->
                            tail.ellipsis
                            |> Option.map (fun _ -> failwith "todo error" (* double ellipsis *))
                            |> Option.defaultWith (fun () -> Pattern.fromDatum ellipsis identifiers head)
                            |> Result.map (fun head -> { patterns = []; ellipsis = Some (head, tail.patterns) })
                        )
                    | Datum.Pair (head, tail) ->
                        Pattern.fromDatum ellipsis identifiers head
                        |> Result.bind (fun head ->
                            loop tail
                            |> Result.map (fun tail -> { tail with patterns = head :: tail.patterns })
                        )
                    | Datum.Null -> Ok { patterns = []; ellipsis = None }
                    | _ -> failwith "todo error"

                loop datum

        type Match =
            | Single of Datum
            | Ellipsis of Match list

        type Template =
            | Null
            | Constant of value: Datum
            | Identifier of name: string
            | Pair of head: Template * tail: Template // (head tail)
            | Ellipsis of tail: Template // (... tail)
            | EllipsisPair of head: Template * tail: Template // (head ... tail)

            static member Variables (template : Template) : Set<string> =
                match template with
                | Null | Constant _ -> Set.empty
                | Identifier name -> Set.singleton name
                | Pair (head, tail) | EllipsisPair (head, tail) -> Set.union (Template.Variables head) (Template.Variables tail)
                | Ellipsis tail -> Template.Variables tail

            static member fromDatum (ellipsis: string) (variables : Set<string>) (datum: Datum) : EvalResult<Template> =
                let recurse = Template.fromDatum ellipsis variables
                match datum with
                | Datum.Null -> Ok Null
                | Datum.Symbol name when name <> ellipsis ->
                    name
                    |> if Set.contains name variables then Identifier else Constant << Datum.Symbol
                    |> Ok
                | Datum.Pair (Datum.Symbol name, tail) when name = ellipsis ->
                    recurse tail
                    |> Result.map Ellipsis
                | Datum.Pair (head, Datum.Pair (Datum.Symbol name, tail)) when name = ellipsis ->
                    recurse head
                    |> Result.bind (fun head ->
                        recurse tail
                        |> Result.map (fun tail -> (head, tail)))
                    |> Result.map EllipsisPair
                | Datum.Pair (head, tail) ->
                    recurse head
                    |> Result.bind (fun head ->
                        recurse tail
                        |> Result.map (fun tail -> (head, tail)))
                    |> Result.map Pair
                | _ -> Ok <| Constant datum

            member this.instantiate(identifiers: Map<string, Match>) : Datum =
                match this with
                | Constant datum -> datum
                | Identifier name ->
                    match Map.tryFind name identifiers with
                    | Some (Single value) -> value
                    | _ -> failwith "unreachable"
                | Pair (head, tail) ->
                    let head = head.instantiate identifiers
                    let tail = tail.instantiate identifiers
                    Datum.Pair(head, tail)
                | Ellipsis _ -> failwith "todo"
                | EllipsisPair (head, tail) ->
                    let variables = Template.Variables head |> Set.toArray
                    variables |> Array.map (fun variable ->
                        match Map.find variable identifiers with
                        | Match.Ellipsis values -> values |> List.map (fun entry -> Map [ (variable, entry) ])
                        | _ -> failwith "todo error"
                    )
                    |> Array.reduce (fun maplist1 maplist2 ->
                        List.zip maplist1 maplist2 |> List.map (fun (map1, map2) -> map_merge map1 map2)
                    )
                    |> List.fold (fun datum identifiers -> Datum.Pair (head.instantiate identifiers, datum)) Datum.Null
                | Null -> Datum.Null
