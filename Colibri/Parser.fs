module rec Colibri.Parser

open Colibri.Lexer
open Colibri.Numerics
open Colibri.Reporting
open Colibri.Span
open Colibri.Objects

type ParseErrorKind =
    | UnexpectedToken of expected: TokenKind * got: TokenKind
    | InvalidToken of got: TokenKind
    | InvalidByteValue of got: Number

type ParseError =
    { loc: FileSpan
      kind: ParseErrorKind }

    interface IReportable with
        member this.GenerateReport =
            match this.kind with
            | UnexpectedToken (expected, got) ->
                Report.Error
                    {| Text = $"Expected token `%A{expected}`"
                       Note = ReportNote.New $"Got `%A{got}`" this.loc |}
            | InvalidToken got ->
                Report.Error
                    {| Text = "Unexpected token"
                       Note = ReportNote.New $"Got `%A{got}`" this.loc |}
            | InvalidByteValue got ->
                Report.Error
                    {| Text = $"Invalid byte value `%A{got}` in byte vector"
                       Note =
                        ReportNote.New "must be an exact, non-negative integer with a value less than 256" this.loc |}

type ParseResult<'T> = Result<'T, ParseError>

let parse_datum (tokens: Token seq) : ParseResult<Datum * Token seq> =
    parse_syntax tokens
    |> Result.map (fun (syn, tokens) -> (syntax_to_datum syn, tokens))

let parse_syntax (tokens: Token seq) : ParseResult<Datum * Token seq> =
    token_split tokens
    |> Result.bind (fun (token, tokens) ->
        let return_syntax (loc: FileSpan) (datum: Datum) = (datum.AsSyntax loc, tokens) |> Ok

        match token.value with
        | TokenKind.BooleanTrue -> return_syntax token.span (Datum.Boolean true)
        | TokenKind.BooleanFalse -> return_syntax token.span (Datum.Boolean false)
        | TokenKind.Number value -> return_syntax token.span (Datum.Number value)
        | TokenKind.Char c -> return_syntax token.span (Datum.Char c)
        | TokenKind.String s -> return_syntax token.span (Datum.String s)
        | TokenKind.Identifier n -> return_syntax token.span (Datum.Symbol n)
        | TokenKind.BeginVector -> parse_vector token.span tokens
        | TokenKind.BeginList -> parse_list tokens
        | TokenKind.BeginByteVector -> parse_bytevector token.span tokens
        | TokenKind.Quote -> make_abbreviation tokens token.span (Datum.Symbol "quote")
        | TokenKind.QuasiQuote -> make_abbreviation tokens token.span (Datum.Symbol "quasiquote")
        | TokenKind.Unquote -> make_abbreviation tokens token.span (Datum.Symbol "unquote")
        | TokenKind.SpliceUnquote -> make_abbreviation tokens token.span (Datum.Symbol "unquote-splicing")
        | TokenKind.EndList
        | TokenKind.Dot ->
            { loc = token.span
              kind = InvalidToken token.value }
            |> Error
        | TokenKind.ExpressionComment
        | TokenKind.LineComment
        | TokenKind.NestedComment -> failwith "unreachable: comment was not filtered")

let make_abbreviation tokens (loc: FileSpan) symbol =
    parse_syntax tokens
    |> Result.map (fun (syntax, tokens) ->
        ((Datum.List [ symbol; syntax ])
            .AsSyntax(loc.Merge syntax.GetLocation.Value),
         tokens))


let parse_vector (begin_location: FileSpan) (tokens: Token seq) : ParseResult<Datum * Token seq> =
    let rec loop (tokens: Token seq) (data: Datum list) =
        token_split tokens
        |> Result.bind (fun (token, tail_tokens) ->
            match token.value with
            | TokenKind.EndList ->
                ((List.rev data |> Array.ofList |> Datum.Vector)
                    .AsSyntax(begin_location.Merge token.span),
                 tail_tokens)
                |> Ok
            | _ ->
                match (parse_syntax tokens) with
                | Ok (datum, tokens) -> loop tokens (datum :: data)
                | Error e -> Error e)

    loop tokens List.empty

let parse_list (tokens: Token seq) : ParseResult<Datum * Token seq> =
    let rec build_list data tail =
        match data with
        | head :: data ->
            build_list
                data
                (Datum
                    .Pair(head, tail)
                    .AsSyntax(head.GetLocation.Value.Merge tail.GetLocation.Value))
        | [] -> tail

    let rec loop (tokens: Token seq) (data: Datum list) : ParseResult<Datum * Token seq> =
        token_split tokens
        |> Result.bind (fun (token, tail_tokens) ->
            match token.value with
            | TokenKind.EndList -> Ok((build_list data (Datum.Null.AsSyntax token.span)), tail_tokens)
            | TokenKind.Dot ->
                parse_syntax tail_tokens
                |> Result.bind (fun (datum, tokens) ->
                    expect_token tokens TokenKind.EndList
                    |> Result.map (fun tokens -> ((build_list data datum), tokens)))
            | _ ->
                parse_syntax tokens
                |> Result.bind (fun (datum, tokens) -> loop tokens (datum :: data)))

    loop tokens []

let parse_bytevector (begin_token: FileSpan) (tokens: Token seq) : ParseResult<Datum * Token seq> =
    let rec loop (tokens: Token seq) (data: byte array) =
        token_split tokens
        |> Result.bind (fun (token, tail_tokens) ->
            match token.value with
            | TokenKind.EndList ->
                (Datum
                    .ByteVector(data)
                     .AsSyntax(begin_token.Merge token.span),
                 tail_tokens)
                |> Ok
            | TokenKind.Number n ->
                match n with
                | Number.Int i when Integer(0) <= i && i <= Integer(255) ->
                    loop tail_tokens (Array.append data [| byte i |])
                | n ->
                    { loc = token.span
                      kind = InvalidByteValue n }
                    |> Error
            | t ->
                { loc = token.span
                  kind = UnexpectedToken(TokenKind.EndList, t) }
                |> Error)

    loop tokens Array.empty

let expect_token (tokens: Token seq) (expected: TokenKind) : ParseResult<Token seq> =
    token_split tokens
    |> Result.bind (fun (token, tokens) ->
        if token.value = expected then
            tokens |> Ok
        else
            { loc = token.span
              kind = UnexpectedToken(expected, token.value) }
            |> Error)

let token_split (tokens: Token seq) : ParseResult<Token * Token seq> =
    let head = Seq.head tokens
    let tail = Seq.tail tokens

    match head.value with
    | TokenKind.NestedComment
    | TokenKind.LineComment -> token_split tail
    | TokenKind.ExpressionComment ->
        parse_syntax tail
        |> Result.bind (fun (_, tokens) -> token_split tokens)
    | _ -> Ok(head, tail)

let token_head tokens = token_split tokens |> Result.map fst

let token_tail tokens = token_split tokens |> Result.map snd
