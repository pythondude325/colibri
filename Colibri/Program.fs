﻿open System
open System.IO

open System.Reflection.Metadata
open Colibri
open Colibri.Objects.Eval
open Objects
open Objects.Eval
open Lexer
open Parser
open Writer
open Colibri.Reporting
open Colibri.Span

let rec readlines () : string seq =
    seq {
        printf "> "
        let line = Console.ReadLine()

        if line <> null then
            yield line
            yield! readlines ()
    }

let builtin_environment =
    NameSource.LocalScopeFromMap (Map [
          // ("a", ref (Datum.Boolean true))
          ("values",
           Datum.Procedure(Procedure.Builtin(Array.ofList >> Ok))
           |> ref)
          ("cons",
           Datum.Procedure(
               Procedure.Builtin(
                   (function
                   | [ a; b ] -> [| Datum.Pair(a, b) |]
                   | _ -> failwith "todo still have to allow errors somehow")
                   >> Ok
               )
           )
           |> ref) ])
    |> Environment.Null.Extend

let try_parse (input: string) : unit =
    printfn $"%A{input}"

    let input_source = { name = "<repl input>"; contents = input } 
    
    let reporter = ConsoleReporter()
    
    try
        let tokens =
            Lexer.FromFileSource input_source
            |> Seq.map (function
                | Ok token -> token
                | Error e ->
                    reporter.Report e
                    failwith "lexer error")
            |> List.ofSeq
            
        let datum =
            match parse_datum tokens with
            | Ok (datum, _) ->
                printfn $"parse   - {write_string datum}"
                datum
            | Error e ->
                reporter.Report e
                failwith "parser error"
                
        let eval_result =
            eval datum builtin_environment

        printfn $"eval    - %A{eval_result}"

        match eval_result with
        | Ok data ->
            for datum in data do
                printfn $"written - {write_string datum}"
        | _ -> ()
    with
    | error ->
        let current_color = Console.ForegroundColor
        Console.ForegroundColor <- ConsoleColor.Red
        printf $"%A{error}"
        Console.ForegroundColor <- current_color
        printfn ""

let repl () : int =
    readlines () |> Seq.iter try_parse; 0

let runfile (filename: string) : int =
    File.ReadAllText filename |> try_parse; repl ()

[<EntryPoint>]
let main (argv: string array) =
    printfn "Hello world"

    match argv with
    | [||] ->
        try_parse "#(abc-xyz #t #f #;( #f ))"

        try_parse "(let-syntax ((test (syntax-rules () ((test) #f)))) (test))"

        try_parse "(let-syntax ((test (syntax-rules () ((test a) (a a))))) (test 1))"

        try_parse "(let-syntax ((test (syntax-rules ... () ((test a ...) (a ...))))) (test a b c))"

        (*  macro system stress test :3

            http://community.schemewiki.org/?scheme-faq-macros#H-se23pq
            the pattern
                `(a (b c ...) (d e ...) ...)`
            can be expanded into the template
                `((a c ...) (b d ...) (e ...) ...)`
        *)
        try_parse "(let-syntax ((test (syntax-rules ... () ((test a (b c ...) (d e ...) ...) ((a c ...) (b d ...) (e ...) ...))))) (test a (b c e) (f g i j) (k l m)))"

        repl ()
    | [| filename |] -> runfile filename
    | _ ->
        eprintf "usage: colibric [filename]"
        1
