module Colibri.Reporting

open System
open Colibri.Span

[<RequireQualifiedAccess>]
type ReportLevel =
    | Error
    | Warning
    | Help
    
    member this.Color =
        match this with
        | Error -> ConsoleColor.Red
        | Warning -> ConsoleColor.Yellow
        | Help -> ConsoleColor.Green

[<Struct>]
type ReportNote =
    { location: FileSpan
      text: string }
    
    static member New (text: string) (location: FileSpan) : ReportNote = { text = text; location = location }
    
    static member LocationOnly = ReportNote.New ""

[<Struct>]
type Report =
    { level: ReportLevel
      text: string
      notes: ReportNote array }
    
    static member Error (values: {| Text: string; Notes: ReportNote array |}) =
        { level = ReportLevel.Error
          text = values.Text
          notes = values.Notes }
        
    static member Error (values: {| Text: string; Note: ReportNote |}) =
        Report.Error {| Text = values.Text; Notes = [| values.Note |] |}
        
    static member Error (values : {| Text: string; Location: FileSpan |}) =
        Report.Error {| Text = values.Text; Note = ReportNote.LocationOnly values.Location |}
        
type IReportable =
    abstract member GenerateReport : Report

type IReporter =
    abstract member Report: IReportable -> unit

type ConsoleReporter() =
    member this.Report(reportable: IReportable) : unit =
        let console_color = Console.ForegroundColor
        
        let report = reportable.GenerateReport

        Console.ForegroundColor <- report.level.Color
        printfn $"{report.level}: {report.text}"
        Console.ForegroundColor <- console_color

        for note in report.notes do
            let loc = note.location
            
            let (line, col) =
                loc.source.GetLineCol loc.start

            printfn $"--> {loc.source.name}:{line}:{col}"

            let lines = loc.GetLines

            for lineno, offset, line in lines do
                printfn $" %4d{lineno} | %s{line}"

                let highlight = String.init line.Length (fun i -> if loc.Contains(i + offset) then "^" else " ")
                

                printf "      | "
                Console.ForegroundColor <- report.level.Color
                printf $"{highlight.TrimEnd()} {note.text}"
                Console.ForegroundColor <- console_color
                printfn ""



    interface IReporter with
        member this.Report r = this.Report r
