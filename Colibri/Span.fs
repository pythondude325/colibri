module rec Colibri.Span

open System

type FileSource =
    { name: string
      contents: string }
    
    
    member this.GetLineCol (offset: int) : int * int =
        this.contents
        |> Seq.take offset
        |> Seq.fold
            (fun (line, col) c ->
                if c = '\n' then
                    (line + 1, 1)
                else
                    (line, col + 1))
            (1,1)
    
    
let lines (chars: char seq) : string seq =
    Seq.unfold
        (function
         | None -> None
         | Some state ->
            let rec loop (chars: char seq) (line: char array): string * char seq option =
                match Seq.tryHead chars with
                | Some '\n' -> (String(line), Some (Seq.tail chars))
                | Some c ->
                    loop (Seq.tail chars) (Array.append line [| c |])
                | None ->
                    (String(line), None)
            
            Some (loop state Array.empty))
        (Some chars)
   
   
let fold_map (folder: 'State -> 'T -> ('U * 'State)) (state: 'State) (s: 'T seq) : 'U seq =
    Seq.unfold
        (fun (s, state) ->
            Seq.tryHead s
            |> Option.map
                (fun item ->
                    let result, next_state = folder state item
                    (result, ((Seq.tail s), next_state))))
        (s, state)
   
let line_offsets (lines: string seq) : (string * int) seq =
    fold_map
        (fun offset line ->
            ((line, offset), (offset + line.Length + 1)))
        0
        lines

[<Struct>]
type FileSpan =
    { source: FileSource
      start: int
      length: int }
    
    member this.End : int =
        this.start + this.length
    
    member this.Contains (pos: int) : bool =
        this.start <= pos && pos < this.End

    member this.GetLines : (int * int * string) array =
        let start = this.start
        let stop = this.End
        
        lines this.source.contents
        |> line_offsets
        |> Seq.indexed
        |> Seq.skipWhile (fun (_lineno, (line, offset)) -> line.Length + offset < start)
        |> Seq.takeWhile (fun (_lineno, (_line, offset)) -> offset <= stop)
        |> Seq.map (fun (lineno, (line, offset)) -> (lineno + 1, offset, line))
        |> Array.ofSeq
    
    member this.Merge(other: FileSpan) : FileSpan =
        assert (this.source = other.source) // Merged spans must have the same source
        
        let this_end = this.start + this.length
        let other_end = other.start + other.length
        let result_end = max this_end other_end

        let result_start =
            min this.start other.start

        { source = this.source
          start = result_start
          length = result_end - result_start }
        
    member this.With(value: 'T) : Spanned<'T> =
        { span = this; value = value }
        

type Spanned<'T> =
    { span: FileSpan
      value: 'T }

    member this.map(mapping: 'T -> 'U) : Spanned<'U> =
        { span = this.span
          value = mapping this.value }

    member this.bind (binder: 'T -> Spanned<'U>) (spanned: Spanned<'T>) : Spanned<'U> =
        let mapped = binder this.value

        { span = this.span.Merge mapped.span
          value = mapped.value }
