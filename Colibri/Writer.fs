module Colibri.Writer

open System.IO
open Colibri.Numerics
open Colibri.Objects
open Colibri.Lexer

let seq_all = Seq.fold (&&) true

type IOError = unit

let rec make_tokens (datum: Datum) : TokenKind seq =
    match datum with
    | Datum.Boolean true -> Seq.singleton TokenKind.BooleanTrue
    | Datum.Boolean false -> Seq.singleton TokenKind.BooleanFalse
    | Datum.Char c -> Seq.singleton (TokenKind.Char c)
    | Datum.String s -> Seq.singleton (TokenKind.String s)
    | Datum.Null ->
        [ TokenKind.BeginList
          TokenKind.EndList ]
    | Datum.Symbol name -> Seq.singleton (TokenKind.Identifier name)
    | Datum.Pair _ ->
        let rec loop data out =
            match data with
            | Datum.Null ->
                seq {
                    yield! out
                    yield TokenKind.EndList
                }
            | Datum.Pair (head, tail) ->
                loop
                    tail
                    (seq {
                        yield! out
                        yield! make_tokens head
                    })
            | _ ->
                seq {
                    yield! out
                    yield TokenKind.Dot
                    yield! make_tokens data
                    yield TokenKind.EndList
                }

        loop datum [ TokenKind.BeginList ]
    | Datum.ByteVector arr ->
        seq {
            yield TokenKind.BeginByteVector

            for b in arr do
                yield TokenKind.Number(Number.Int(Integer b))

            yield TokenKind.EndList
        }
    | Datum.Vector values ->
        seq {
            yield TokenKind.BeginVector

            for value in values do
                yield! make_tokens value

            yield TokenKind.EndList
        }
    | Datum.Number value -> Seq.singleton (TokenKind.Number value)
    | Datum.Procedure _ -> failwith "cannot write procedures"
    | Datum.Syntax _ -> failwith "cannot write syntax"

let token_string (token: TokenKind) =
    match token with
    | TokenKind.BeginList -> "("
    | TokenKind.EndList -> ")"
    | TokenKind.BeginVector -> "#("
    | TokenKind.BeginByteVector -> "#u8("
    | TokenKind.Dot -> "."
    | TokenKind.Quote -> "'"
    | TokenKind.QuasiQuote -> "`"
    | TokenKind.Unquote -> ","
    | TokenKind.SpliceUnquote -> ",@"
    | TokenKind.BooleanFalse -> "#false"
    | TokenKind.BooleanTrue -> "#true"
    | TokenKind.ExpressionComment -> "#;"
    | TokenKind.Identifier i ->        
        let normal_symbol: bool =
            match i with
            | "+inf.0"
            | "-inf.0"
            | "+nan.0"
            | "-nan.0" -> true
            | _ ->
                match Seq.tryHead i with
                | Some c when identifier_initial c ->
                    i |> Seq.tail |> Seq.map identifier_subsequent |> seq_all  
                | Some '-'
                | Some '+' ->
                    match Seq.tryHead (Seq.tail i) with
                    | Some '.' ->
                        match i |> Seq.skip 2 |> Seq.tryHead with
                        | Some c when identifier_dot_subsequent c ->
                            i |> Seq.skip 3 |> Seq.map identifier_subsequent |> seq_all
                        | _ -> false
                    | Some c when identifier_sign_subsequent c ->
                        i |> Seq.skip 2 |> Seq.map identifier_subsequent |> seq_all
                    | None -> true
                    | Some _ -> false
                | Some '.' ->
                    match Seq.tryHead (Seq.tail i) with
                    | Some c when identifier_dot_subsequent c ->
                        i |> Seq.skip 2 |> Seq.map identifier_subsequent |> seq_all
                    | _ -> false
                | _ -> false
        
        if normal_symbol then
            i
        else
            i
            |> String.collect
                (function
                 | '|' -> "\|"
                 | '\a' -> "\\a"
                 | '\b' -> "\\b"
                 | '\t' -> "\\t"
                 | '\n' -> "\\n"
                 | '\r' -> "\\r"
                 | c -> string(c))
            |> (fun s -> $"|{s}|")
    | TokenKind.Char c ->
        match c with
        | '\a' -> "#\\alarm"
        | '\b' -> "#\\backspace"
        | '\x7f' -> "#\\delete"
        | '\x1b' -> "#\\escape"
        | '\n' -> "#\\newline"
        | '\x00' -> "#\\null"
        | '\r' -> "#\\return"
        | ' ' -> "#\\space"
        | '\t' -> "#\\tab"
        | _ -> $"#\\{c}"
    | TokenKind.Number n ->
        match n with
        | Number.Int i -> i.ToString()
        | Number.Rational r -> r.ToString()
        | _ -> failwith "todo"
    | TokenKind.String s ->
        s
        |> String.collect
            (function
            | '"' -> "\\\""
            | '\\' -> "\\\\"
            | '\a' -> "\\a"
            | '\b' -> "\\b"
            | '\t' -> "\\t"
            | '\n' -> "\\n"
            | '\r' -> "\\r"
            | c -> string(c))
        |> (fun s -> $"\"{s}\"")
    | TokenKind.LineComment
    | TokenKind.NestedComment -> ""

let token_between (a: TokenKind, b: TokenKind) : bool =
    match (a, b) with
    | _, TokenKind.EndList -> false
    | (TokenKind.BeginList
      | TokenKind.BeginVector
      | TokenKind.BeginByteVector),
      _ -> false
    | TokenKind.NestedComment, _ -> false
    | TokenKind.Quote, _ -> false
    | _ -> true

let write_tokens (tokens: TokenKind list) (writer: TextWriter) =
    let rec loop tokens =
        match tokens with
        | a :: (rest & b :: _) ->
            writer.Write(token_string a)

            if token_between (a, b) then
                writer.Write ' '

            loop rest
        | a :: rest ->
            writer.Write(token_string a)
            loop rest
        | [] -> ()

    loop tokens

let write_string (datum: Datum) =
    let writer = new StringWriter()
    write_tokens (List.ofSeq (make_tokens datum)) writer 
    writer.ToString()
